package com.example.auth.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
